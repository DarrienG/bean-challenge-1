extern crate actix_web;
extern crate difflib;
extern crate indoc;
#[macro_use]
extern crate serde_derive;
use actix_web::http::StatusCode;
use actix_web::{http, server, App, HttpRequest, HttpResponse, Json, Result};
use difflib::sequencematcher::SequenceMatcher;
use indoc::indoc;

static WEB_URL: &str = "https://beanpuzzle.labs.darrien.dev/";
static FAIL_URL: &str = "https://darrienglasser.com/";
static ANSWER: &str = "answer";
static INTRO: &str = "intro";
static REVERSE: &str = "reverse";
static BASE64: &str = "base64decode";
static HEART: &str = "hencode";
static HOTTEST: &str = "hottest";

#[derive(Deserialize, Serialize)]
struct Beanswer {
    beanswer: String,
}

#[derive(Deserialize, Serialize)]
struct Beansponse {
    correct: bool,
    message: String,
    next: String,
}

fn entry_look(_: &HttpRequest) -> Result<HttpResponse> {
    let intro_string = indoc!(
        "
        Bean puzzle is activated.
        Each page implements a GET which has the puzzle.
        To answer the puzzle, post to the same URL with /answer at the end.

        Answers to the bean puzzle are given via POST with a JSON structure that looks like so:
        {
            \"beanswer\" : \"My answer to the bean question\"
        }

        Responses are given with a JSON structure that contains the correctness of your answer \
        and the following link to go to.

        Example structure:
        {
            \"correct\"\t:\t\"False\",
            \"message\"\t:\t\"Intruder alert :(\",
            \"next\"\t:\t\"darrienglasser.com\"
        }

        Clues for the answer are sometimes in the URL, and somemtimes in the text.
        Try it and send a Beanswer to https://beanpuzzle.labs.darrien.dev/intro/answer
        with your first name to go to the next step.
        "
    );
    Ok(HttpResponse::build(StatusCode::OK)
        .content_type("text/plain; charset=utf-8")
        .body(intro_string))
}

fn entry_answer(answer: Json<Beanswer>) -> Json<Beansponse> {
    let bean = &answer.beanswer;
    if bean.to_lowercase().contains("vrinda") {
        Json(Beansponse {
            correct: true,
            message: "Hi my bean :)".to_string(),
            next: format!("{}{}", WEB_URL, REVERSE),
        })
    } else {
        Json(Beansponse {
            correct: true,
            message: "This isn't bean :(".to_string(),
            next: FAIL_URL.to_string(),
        })
    }
}

fn rev_look(_: &HttpRequest) -> Result<HttpResponse> {
    Ok(HttpResponse::build(StatusCode::OK)
        .content_type("text/plain; charset=utf-8")
        .body("peanut"))
}

fn rev_answer(answer: Json<Beanswer>) -> Json<Beansponse> {
    if &answer.beanswer == "tunaep" {
        Json(Beansponse {
            correct: true,
            message: "Yas my queen, slay".to_string(),
            next: format!("{}{}", WEB_URL, BASE64),
        })
    } else {
        Json(Beansponse {
            correct: false,
            message: "nud".to_string(),
            next: FAIL_URL.to_string(),
        })
    }
}

fn b64_look(_: &HttpRequest) -> Result<HttpResponse> {
    Ok(HttpResponse::build(StatusCode::OK)
        .content_type("text/plain; charset=utf-8")
        .body("SGkgbXkgbG92ZQo="))
}

fn b64_answer(answer: Json<Beanswer>) -> Json<Beansponse> {
    if &answer.beanswer == "Hi my love" {
        Json(Beansponse {
            correct: true,
            message: "That's kinda right tho".to_string(),
            next: format!("{}{}", WEB_URL, HEART),
        })
    } else {
        Json(Beansponse {
            correct: false,
            message: "Beeeeeeeeeeeeeeean".to_string(),
            next: FAIL_URL.to_string(),
        })
    }
}

fn heart_look(_: &HttpRequest) -> Result<HttpResponse> {
    Ok(HttpResponse::build(StatusCode::OK)
        .content_type("text/plain; utf-8")
        .body("U+1F496 please"))
}

fn heart_answer(answer: Json<Beanswer>) -> Json<Beansponse> {
    if answer.beanswer.contains("💖") {
        Json(Beansponse {
            correct: true,
            message: "Thank yous my bean".to_string(),
            next: format!("{}{}", WEB_URL, HOTTEST),
        })
    } else {
        Json(Beansponse {
            correct: false,
            message: "Bean I need :(".to_string(),
            next: FAIL_URL.to_string(),
        })
    }
}

fn hottest_look(_: &HttpRequest) -> Result<HttpResponse> {
    Ok(HttpResponse::build(StatusCode::OK)
        .content_type("textplain; utf-8")
        .body("Who is the hottest girl in the world?"))
}

fn hottest_answer(answer: Json<Beanswer>) -> Json<Beansponse> {
    let mut matcher = SequenceMatcher::new("my desi girl my desi girl", &answer.beanswer);
    let similarity = matcher.ratio();
    if similarity > 0.6 {
        Json(Beansponse {
            correct: true,
            message: "Yas my bean ;)".to_string(),
            next: "https://vrinda.dev".to_string(),
        })
    } else {
        Json(Beansponse {
            correct: false,
            message: "Thumka lagaike she'll rock your world".to_string(),
            next: FAIL_URL.to_string(),
        })
    }
}

fn main() {
    let port = 7236;
    println!("Activated bean puzzle on port {}", port);
    server::new(|| {
        App::new()
            .resource(&format!("/{}", INTRO), |r| {
                r.method(http::Method::GET).f(entry_look)
            })
            .resource(&format!("/{}/{}", INTRO, ANSWER), |r| {
                r.method(http::Method::POST).with(entry_answer)
            })
            .resource(&format!("/{}", REVERSE), |r| {
                r.method(http::Method::GET).f(rev_look)
            })
            .resource(&format!("/{}/{}", REVERSE, ANSWER), |r| {
                r.method(http::Method::POST).with(rev_answer)
            })
            .resource(&format!("/{}", BASE64), |r| {
                r.method(http::Method::GET).f(b64_look)
            })
            .resource(&format!("/{}/{}", BASE64, ANSWER), |r| {
                r.method(http::Method::POST).with(b64_answer)
            })
            .resource(&format!("/{}", HEART), |r| {
                r.method(http::Method::GET).f(heart_look)
            })
            .resource(&format!("/{}/{}", HEART, ANSWER), |r| {
                r.method(http::Method::POST).with(heart_answer)
            })
            .resource(&format!("/{}", HOTTEST), |r| {
                r.method(http::Method::GET).f(hottest_look)
            })
            .resource(&format!("/{}/{}", HOTTEST, ANSWER), |r| {
                r.method(http::Method::POST).with(hottest_answer)
            })
    })
    .bind(format!("127.0.0.1:{}", port))
    .unwrap()
    .run();
}
